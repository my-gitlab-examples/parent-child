# parent-child

This is an example of GitLab's [Child / Parent Pipelines](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#child--parent-pipelines).

> When you set a rule for a trigger in the parent's configuration file, you should also put it in the child's configuration file.

```yml
# .gitlab-ci.yml

...

service_one:
  stage: triggers
  trigger:
    include: 
      - local: services/service-one/.gitlab-ci.yml
    strategy: depend
  # these are the trigger's rules.
  rules:
    - changes:
      - services/service-one/*

...

```

```yml
# services/service-one/.gitlab-ci.yml

...

test:
  stage: test
  script:
    - echo '[Service One] Test'
  # these rules should match the trigger's rules.
  rules:
    - changes:
      - services/service-one/*

...

```
